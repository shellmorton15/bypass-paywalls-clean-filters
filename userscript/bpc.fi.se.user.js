// ==UserScript==
// @name            Bypass Paywalls Clean - fi/se
// @version         2.8.0.6
// @downloadURL     https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.fi.se.user.js
// @updateURL       https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.fi.se.user.js
// @match           *://*.hs.fi/*
// @match           *://*.iltalehti.fi/*
// @match           *://*.nyteknik.se/*
// @match           *://*.suomensotilas.fi/*
// ==/UserScript==

(function() {
  'use strict';

window.setTimeout(function () {

var domain;

if (matchDomain('iltalehti.fi')) {
  let ads = document.querySelectorAll('div[class^="p2m385-"], div#anop-container, .ad, div.iZivCJ');
  for (let ad of ads)
    ad.setAttribute('style', 'display:none;');
  let paywall = document.querySelector('div.faded-text');
  if (paywall) {
    let scripts = document.querySelectorAll('script');
    let json_script;
    for (let script of scripts) {
      if (script.innerText.includes('window.App=')) {
        json_script = script;
        break;
      }
    }
    if (json_script) {
      let json = json_script.innerHTML.split('window.App=')[1].split('</script')[0];
      json = json.replace(/undefined/g, '"undefined"');
      let json_article = JSON.parse(json).state.articles;
      if (!Object.keys(json_article).length)
        window.location.reload(true);
      if (Object.keys(json_article).length) {
        paywall.remove();
        let url_loaded = Object.keys(json_article)[0];
        if (url_loaded && !window.location.pathname.includes(url_loaded))
          window.location.reload(true);
        let pars = Object.values(json_article)[0].items.body;
        let content = document.querySelector('div.article-body');
        if (content && pars) {
          function ilta_par_text(items) {
            let par_text = '';
            for (let item of items) {
              if (item.text)
                par_text += item.text;
              else if (item.name)
                par_text += '<a href="/henkilot/' + item.name + '">' + item.name + '</a>';
              else if (item.type === 'link')
                par_text += '<a href="' + item.url + '" target="_blank">' + item.items[0].text + '</a>';
              else if (item.type) {
                let item_type = '<' + item.type[0] + '>';
                par_text += item.items.map(i => item_type + i.text + item_type.replace('<', '</')).join('');
              } else if (item[0]) { //aside-list
                par_text += item[0].text;
              }
            }
            return par_text;
          }
          function ilta_wrap_list(elem, par_text) {
            if (par_text) {
              elem += '<div class="article-bullets"><ul>';
              elem += par_text;
              elem += '</ul></div>';
            }
            return elem;
          }
          content.innerHTML = '';
          let article_new = '';
          for (let par of pars) {
            let elem = '';
            let par_text = '';
            let par_ignore = false;
            if (par.type === 'paragraph') {
              par_text = ilta_par_text(par.items);
              if (par_text)
                elem = '<p class="paragraph">' + par_text + '</p>';
            } else if (par.type === 'subheadline') {
              if (par.text)
                elem = '<h3 class="subheadline" style="margin:20px;">' + par.text + '</h3>';
            } else if (par.type === 'aside') {
              elem = '<div class="aside-container"><div class="aside">';
              for (let item of par.items) {
                if (item.text || (item.type === 'paragraph' && item.items)) {
                  if (par_text) {
                    elem = ilta_wrap_list(elem, par_text);
                    par_text = '';
                  }
                  if (item.text)
                    elem += '<h3 class="subheadline" style="margin:20px;">' + item.text + '</h3>';
                  else {
                    let par_text_sub = ilta_par_text(item.items);
                    if (par_text_sub)
                      elem += '<p class="paragraph">' + par_text_sub + '</p>';
                  }
                } else if (item.type === 'list') {
                  let par_text_sub = ilta_par_text(item.items);
                  if (par_text_sub) {
                    par_text += '<li>';
                    par_text += par_text_sub;
                    par_text += '</li>';
                  }
                }
              }
              if (par_text)
                elem = ilta_wrap_list(elem, par_text);
              elem += '</div></div>';
            } else if (par.type === 'blockquote') {
              elem = '<p style="font-size: 1.2em; margin:20px;"><i>&quot;' + par.items.map(i => i.text).join('') + '&quot;</i></p>';
            } else if (par.type === 'divider') {
              elem = '<div class="article-divider"><div class="article-divider-content"></div></div>';
            } else if (par.type.toLowerCase() === 'list') {
              elem = '<div class="article-bullets"><ul>';
              for (let item of par.items)
                elem += '<li>' + item.map(i => i.text).join('') + '</li>';
              elem += '</ul></div>';
            } else if (par.type === 'related-article') {
              elem = '<div class="related-articles related-articles-within-text"><h3>Lue myös</h3><a href="/' + par.article.category.category_name + '/a/' + par.article.article_id + '">' + par.article.title + '</a></div>';
            } else if (par.type === 'image') {
              if (par.urls.default) {
                let caption = par.properties.caption ? par.properties.caption : '';
                let source = par.properties.source ? par.properties.source : '';
                elem = '<p><div><div style="text-align: center;"><img src="' + par.urls.default + '" alt="' + caption + '"></div><div class="media-caption"><span class="caption-text">' + caption + '</span><span class="media-source">' + source + '</span></div></div></p>';
              }
            } else if (par.type === 'embed') {
              elem = par.embed_html;
            } else if (par.type === 'advertisement') {
              par_ignore = true;
            }
            if (elem)
              article_new += elem;
            else if (!par_ignore)
              console.log(par);
          }
          let parser = new DOMParser();
          let par_html = parser.parseFromString('<div>' + article_new + '</div>', 'text/html');
          content.appendChild(par_html.querySelector('div'));
        }
      }
    }
  }
}
else if (matchDomain('hs.fi')) {// dynamic.hs.fi only
  let url = window.location.href;
  if (!url.includes('https://dynamic.hs.fi')) {
    let iframe = document.querySelector('iframe[src^="https://dynamic.hs.fi/a/"]');
    if (iframe && url.includes('.html')) {
      window.setTimeout(function () {
        window.location.href = iframe.src;
      }, 500);
    }
  } else {
    let paywall = document.querySelector('.paywall-container, .paywall-wrapper');
    if (paywall) {
      let scripts = document.querySelectorAll('script');
      let json_script;
      for (let script of scripts) {
        if (script.innerText.includes('window.__NUXT__=')) {
          json_script = script;
          break;
        }
      }
      let json_text;
      if (json_script.innerHTML.includes('paywallComponents:['))
        json_text = json_script.innerHTML.replace(/\r\n/g, '').split('amlData:[')[1].split('metaData')[0].split('paywallComponents:[')[1].slice(0, -4);
      let main = document.querySelector('main');
      if (main && json_text) {
        let pars = json_text.split('{type:');
        let type, value, slides, src, elem, img, caption, caption_text, par_html, par_text;
        let parser = new DOMParser();
        for (let par of pars) {
          elem = '';
          type = par.split(',')[0];
          if (['a', 'i'].includes(type)) { // text
            value = par.split('value:')[1].split('}')[0].replace(/(^"|"$)/g, '');
            if (!value.includes('<p>'))
              value = '<p>' + value + '</p>';
            par_html = parser.parseFromString(value, 'text/html');
            elem = par_html.querySelector('p');
          } else if (['D', 'f', 'j', 'k', 'n'].includes(type)) { // quote
            if (par.includes('text:') && par.includes(',position:')) {
              value = par.split('text:')[1].split(',position:')[0].replace(/(^"|"$)/g, '');
              elem = document.createElement('p');
              elem.innerText = value;
              elem.setAttribute('style', 'font-style: italic;');
            }
          } else if (['l', 'm', 'o', 'u'].includes(type)) { // authors
            if (!par.includes('text:'))
              continue;
            value = par.split('text:')[1].split(/,(role|type)/)[0].replace(/(^"|"$)/g, '');
            if (value.length > 1) {
              elem = document.createElement('p');
              elem.innerText = value;
            }
          } else if (['e', 'h', 'y'].includes(type)) { // image
            if (!par.includes('src:'))
              continue;
            src = par.split('src:"')[1].split('",')[0];
            if (!src.startsWith('http'))
              src = 'https://arkku.mediadelivery.fi/img/468/' + src;
            elem = document.createElement('p');
            img = document.createElement('img');
            img.setAttribute('src', src);
            img.setAttribute('style', 'width:468px !important');
            elem.appendChild(img);
            if (par.includes('caption:')) {
              caption = document.createElement('figcaption');
              caption_text = par.split('caption:')[1].split('",')[0];
              if (caption_text.length)
                caption_text = caption_text.slice(1, caption_text.length - 1);
              caption.innerText = caption_text;
              elem.appendChild(caption);
            }
          } else if (['p', 'r'].includes(type)) { // slides
            slides = par.split('src:');
            elem = document.createElement('p');
            for (let slide of slides) {
              if (slide.includes('.jpg')) {
                src = slide.split(',')[0].replace(/"/g, '');
                if (!src.startsWith('http'))
                  src = 'https://arkku.mediadelivery.fi/img/468/' + src;
                img = document.createElement('img');
                img.setAttribute('src', src);
                img.setAttribute('style', 'width:468px !important');
                elem.appendChild(img);
                caption = document.createElement('figcaption');
                caption_text = slide.includes('text:') ? slide.split('text:')[1].split(',"text-style"')[0] : slide.split('caption:')[1].split('",')[0];
                if (caption_text.length)
                  caption_text = caption_text.slice(1, caption_text.length - 1);
                par_html = parser.parseFromString('<div>' + caption_text + '</div>', 'text/html');
                elem.appendChild(par_html.querySelector('div'));
              }
            }
          } else
            false;//console.log('type: ' + type + ' par: ' + par);
          if (elem) {
            elem.setAttribute('class', 'article-body px-16 mb-24');
            main.appendChild(elem);
          }
        }
        main.appendChild(document.createElement('br'));
      }
      removeDOMElement(paywall);
    }
  }
}

else if (matchDomain('nyteknik.se')) {
  let locked_article = document.querySelector('div.locked-article');
  if (locked_article)
    locked_article.classList.remove('locked-article');
  window.setTimeout(function () {
    let hidden_images = document.querySelectorAll('img[src=""][data-proxy-image]');
    for (let hidden_image of hidden_images)
      hidden_image.setAttribute('src', hidden_image.getAttribute('data-proxy-image').replace('_320', '_640'));
  }, 2000);
}

else if (matchDomain('suomensotilas.fi')) {
  let obscured = document.querySelector('div.epfl-pw-obscured');
  if (obscured)
    obscured.classList.remove('epfl-pw-obscured');
}

}, 1000);

// General Functions

function matchDomain(domains, hostname) {
  var matched_domain = false;
  if (!hostname)
    hostname = window.location.hostname;
  if (typeof domains === 'string')
    domains = [domains];
  domains.some(domain => (hostname === domain || hostname.endsWith('.' + domain)) && (matched_domain = domain));
  return matched_domain;
}

function removeDOMElement(...elements) {
  for (let element of elements) {
    if (element)
      element.remove();
  }
}

})();
